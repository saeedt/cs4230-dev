#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <omp.h>
void Get_args(int argc, char **argv);
void Usage(char* prog_name);
int hp2(int n);
const long MAX_PSIZE = 33554432; // 2^25
const long MAX_THR = 24; // For Fractus; other machines may go higher
float *A; // Global arrays over which DP is done
float *B;
int PSize, Thres, TotThr;//Problem size and Threshold which determines chunking, TotThr allocated
int main(int argc, char **argv) {
  Get_args(argc, argv); printf("Perform DP: PSize = %d, Thres = %d, TotThr = %d\n", PSize, Thres, TotThr);
  omp_set_num_threads(TotThr);
  //printf("test");
  printf("Running with %d Threads\n", omp_get_num_threads());
  A = (float *) malloc(PSize*sizeof(float)); B = (float *) malloc(PSize*sizeof(float));srand(17);
  for (int i=0; i<PSize; ++i) { A[i] = rand()%16; //printf("A[%d] = %f\n", i, A[i]);
                                B[i] = rand()%16; //printf("B[%d] = %f\n", i, B[i]);
  }				
 int dpsum = 0;
#pragma omp parallel num_threads(TotThr) // thr cnt
{
#pragma omp for schedule (static, Thres) reduction(+: dpsum)
  for(int i = 0; i < PSize; i += 1)
    { dpsum = dpsum + A[i] * B[i];
      printf("A[%d]*B[%d] = %e : (%d of %d)\n", i,i, (double) (A[i]*B[i]),
	     omp_get_thread_num(), omp_get_num_threads());    
    }
}
 printf("Dot Product is %d\n", dpsum);
}

void Get_args(int argc, char **argv) {
   if (argc != 4) Usage(argv[0]);
   PSize = strtol(argv[1], NULL, 10);  
   if (PSize <= 0 || PSize > MAX_PSIZE) Usage(argv[0]);
   Thres = strtol(argv[2], NULL, 10);
   if (Thres < 1) Usage(argv[0]);
   TotThr = strtol(argv[3], NULL, 10);
   if (TotThr < 1 || TotThr > MAX_THR) Usage(argv[0]);   
}  

void Usage(char *prog_name) {
   fprintf(stderr, "usage: %s PSize:int Thres:int TotThr:int\n", prog_name);
   fprintf(stderr, "PSize : problem size; Thres : chunking; TotThr : Thr alloc\n");
   exit(0);
}  /* Usage */

int hp2(int n)
{
  int p = (int)log2(n);
  return (int)pow(2, p);
}
